package park;

import park.card.Card;
import park.card.SharedCard;
import park.enums.Actions;
import park.gate.Gate;
import park.gate.SharedGate;
import park.semaphore.SharedSemaphore;
import park.semaphore.StreetSemaphore;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main implements Runnable {
    private static String logfile;
    private Semaphore sem, semSemaphore, semGate, semCard;
    private SharedSemaphore sharedSemaphore;
    private SharedGate sharedGate;
    private SharedCard sharedCard;
    private List<String> codes;
    private int max_spaces, count;
    private JLabel message;

    /**
     * The default constructor of the main class
     *
     * @param sem             the semaphore that controls the gate message
     * @param semSemaphore    the semaphore that controls the StreetSemaphore
     * @param semGate         the semaphore that controls the gate
     * @param semCard         the semaphore that controls the card
     * @param sharedSemaphore the shared obj between the StreetSemaphore and the Main class
     * @param sharedGate      the shared obj between the Gate and the Main class
     * @param sharedCard      the shared obj between the Card and the Main class
     * @param codes           the access codes for the parking lot
     * @param max_spaces      the max spaces available for the parking lot
     */
    public Main(Semaphore sem, Semaphore semSemaphore, Semaphore semGate, Semaphore semCard, SharedSemaphore sharedSemaphore,
                SharedGate sharedGate, SharedCard sharedCard, List<String> codes, int max_spaces) {
        this.sem = sem;
        this.semSemaphore = semSemaphore;
        this.semGate = semGate;
        this.semCard = semCard;
        this.sharedSemaphore = sharedSemaphore;
        this.sharedGate = sharedGate;
        this.sharedCard = sharedCard;
        this.codes = codes;
        this.max_spaces = max_spaces;
        this.count = max_spaces;
    }

    /**
     * Read a file into a String List
     *
     * @param file the file name to be read
     * @return a list with all of the lines read in the file
     */
    private static List<String> readFile(String file) {
        List<String> lines = null;
        try {
            Charset ENCODING = StandardCharsets.UTF_8;
            Path path = Paths.get(file);
            lines = Files.readAllLines(path, ENCODING);
        } catch (IOException e) {
            System.out.println("Ficheiro indisponível");
            System.exit(0);
        }
        return lines;
    }

    public static void main(String[] args) {
        String config = "res/config";
        logfile = "res/log";
        if (args.length > 0) {
            config = args[0];
            if (args[1] != null)
                logfile = args[1];
        }
        List<String> lines = readFile(config);
        final int max_spaces = Integer.parseInt(lines.get(0));
        final List<String> codes = lines.subList(2, lines.size());

        // All of the semaphores
        final Semaphore sem = new Semaphore(0);
        final Semaphore semSemaphore = new Semaphore(0);
        final Semaphore semGate = new Semaphore(0);
        final Semaphore semCard = new Semaphore(0);

        // All of the shared objects
        final SharedSemaphore sharedSemaphore = new SharedSemaphore(Color.GREEN);
        final SharedGate sharedGate = new SharedGate(false);
        final SharedCard sharedCard = new SharedCard();

        // All of the treads
        final Main main = new Main(sem, semSemaphore, semGate, semCard, sharedSemaphore, sharedGate,
                sharedCard, codes, max_spaces);
        final StreetSemaphore semaphore = new StreetSemaphore(sharedSemaphore, semSemaphore);
        final Gate gate = new Gate(sharedGate, semGate, sem);
        final Card card = new Card(sharedCard, semCard);

        final Thread mainThread = new Thread(main);
        final Thread semaphoreThread = new Thread(semaphore);
        final Thread gateThread = new Thread(gate);
        final Thread cardThread = new Thread(card);

        // Starting all of the treads
        mainThread.start();
        semaphoreThread.start();
        gateThread.start();
        cardThread.start();

        // Semaphore release
        semSemaphore.release();
        semGate.release();

    }

    public void setVariableMsg(String m) {
        message.setText(m);
    }

    @Override
    public void run() {
        try {
            JFrame frame = new JFrame("Park");
            frame.setSize(600, 600);
            frame.setVisible(true);
            frame.setLocation(210, 100);

            JPanel panel = new JPanel();
            panel.setLayout(null);

            panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

            JLabel spaces = new JLabel();
            message = new JLabel();

            panel.add(spaces);
            spaces.setBounds(50, 100, 200, 120);

            panel.add(message);
            message.setBounds(50, 150, 200, 120);

            frame.add(panel);

            spaces.setText("Park Available spaces:" + count + "/" + max_spaces);
            message.setText("Cancela fechada!");

            writeToLog("SYSTEM INIT\n");
            while (true) {
                semCard.acquire();
                switch (sharedCard.getAction()) {
                    case CODE:
                        verifyCard();
                        break;
                    case KEY:
                        keyAction();
                        break;
                    case STOP:
                        systemStop();
                        break;
                    case RESET:
                        systemReset();
                        break;
                    case CAR_ON:
                    case CAR_OFF:
                        modifySpaces(sharedCard.getAction());
                        break;
                }
                spaces.setText("Available spaces: " + count + "/" + max_spaces);
            }

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void systemReset() throws IOException {
        writeToLog("SYSTEM RESET");
        semaphoreSwitch(Color.GREEN);
        gateSwitch(false);
        count = max_spaces;
    }

    private void systemStop() throws IOException {
        writeToLog(sharedCard.getSystemState() ? "SYSTEM STOPPED" : "SYSTEM STARTED");
        if (sharedCard.getSystemState()) {
            if (sharedGate.getState()) {
                gateSwitch(false);
            }
            if (sharedSemaphore.getColor() == Color.GREEN) {
                semaphoreSwitch(Color.RED);
            }
        } else {
            if (sharedSemaphore.getColor() == Color.RED) {
                if (count != 0) {
                    semaphoreSwitch(Color.GREEN);
                }
            }
        }
    }

    private void keyAction() throws IOException {
        writeToLog("KEY USED");
        gateSwitch(!sharedGate.getState());
    }

    private void verifyCard() throws IOException {
        writeToLog("CODE INSERTED");
        if (codes.stream().filter(c -> c.equals(sharedCard.getCode())).count() == 1) {
            writeToLog("CODE ACCEPTED");
            gateSwitch(true);
        } else {
            writeToLog("CODE REJECTED");
            showMessage("Codigo Invalido");
        }
    }

    private void modifySpaces(Actions spaces) throws IOException {
        if (sharedGate.getState()) {
            if (spaces == Actions.CAR_ON)
                carOn();
            else
                carOff();
        } else {
            writeToLog("GATE CLOSED");
            showMessage("Cancela fechada");
        }
    }

    private void carOn() throws IOException {
        if (count > 0) {
            count--;
            gateSwitch(false);
            if (count == 0) {
                semaphoreSwitch(Color.RED);
            }
            writeToLog("NEW CAR INSIDE");
        } else {
            writeToLog("NO SPACE FOR CAR");
            showMessage("Sem Espaço");
        }
    }

    private void carOff() throws IOException {
        if (count < max_spaces) {
            count++;
            gateSwitch(false);
            if (count >= 0) {
                semaphoreSwitch(Color.GREEN);
            }
            writeToLog("CAR LEFT");
        } else {
            writeToLog("PARK EMPTY");
            showMessage("Sem Carros");
        }
    }

    private void gateSwitch(boolean isOpened) {
        setVariableMsg(sharedGate.getState() ? "A fechar cancela... " : "A abrir cancela... ");
        sharedGate.setState(isOpened);
        semGate.release();
        try {
            Thread.sleep(1000);
            sem.acquire();
            setVariableMsg(sharedGate.getState() ? "Cancela aberta!" : "Cancela fechada!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            writeToLog(sharedGate.getState() ? "GATE OPENED" : "GATE CLOSED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void semaphoreSwitch(Color color) {
        sharedSemaphore.setColor(color);
        semSemaphore.release();
        try {
            writeToLog(sharedSemaphore.getColor() == Color.GREEN ? "SEMAPHORE GREEN" : "SEMAPHORE RED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showMessage(String m) {
        JOptionPane.showMessageDialog(null, m);
    }

    private void writeToLog(String s) throws IOException {
        Path path = Paths.get(logfile);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
        String date = formatter.format(new Date());

        byte[] strToBytes = (date + " | " + s + "\n").getBytes();
        Files.write(path, strToBytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
    }
}
