package park.card;

import park.enums.Actions;

import javax.swing.*;
import java.util.concurrent.Semaphore;

/**
 * The class responsible for the card
 */
public class Card implements Runnable {
    private Semaphore sem;
    private SharedCard shared;

    public Card(SharedCard shared, Semaphore sem) {
        this.shared = shared;
        this.sem = sem;
    }

    @Override
    public void run() {
        JFrame frame = new JFrame("Card / Key");
        frame.setSize(600, 300);
        frame.setVisible(true);
        frame.setLocation(820, 100);

        JButton btnCard = new JButton("Passagem cartão");
        JButton btnCarOn = new JButton("Entrada carro");
        JButton btnCarOff = new JButton("Saida carro");
        JButton btnKey = new JButton("Chave A/F");
        JButton btnReset = new JButton("Reniciar sistema");
        JButton btnPause = new JButton("Paragem sistema");

        btnKey.addActionListener(l -> {
            shared.setAction(Actions.KEY);
            sem.release();
        });

        btnCard.addActionListener(l -> {
            shared.setAction(Actions.CODE);
            String code = JOptionPane.showInputDialog("Introduza o código: ");
            shared.setCode(code);
            sem.release();
        });

        btnCarOn.addActionListener(l -> {
            shared.setAction(Actions.CAR_ON);
            sem.release();
        });

        btnCarOff.addActionListener(l -> {
            shared.setAction(Actions.CAR_OFF);
            sem.release();
        });

        btnPause.addActionListener(l -> {
            shared.setAction(Actions.STOP);
            shared.setSystemState(!shared.getSystemState());
            if (shared.getSystemState()) {
                btnCard.setEnabled(false);
                btnCarOn.setEnabled(false);
                btnCarOff.setEnabled(false);
                btnKey.setEnabled(false);
                btnReset.setEnabled(false);
            } else {
                btnCard.setEnabled(true);
                btnCarOn.setEnabled(true);
                btnCarOff.setEnabled(true);
                btnKey.setEnabled(true);
                btnReset.setEnabled(true);
            }
            sem.release();
        });

        btnReset.addActionListener(l -> {
            shared.setAction(Actions.RESET);
            sem.release();
        });

        JPanel panel = new JPanel();
        panel.add(btnCard);
        panel.add(btnCarOn);
        panel.add(btnCarOff);
        panel.add(btnKey);
        panel.add(btnReset);
        panel.add(btnPause);

        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        frame.add(panel);
    }
}
