package park.card;

import park.enums.Actions;

/**
 * Shared obj between Main and Card
 */
public class SharedCard {
    private String code;
    private Actions action;
    private boolean systemState;

    public SharedCard() {}

    /**
     * Returns the code inputted by the user
     *
     * @return the string with the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setts the string with the code
     *
     * @param code the code inputted by the user
     */
    public void setCode(String code) {
        this.code = code;
    }

    public Actions getAction() {
        return action;
    }

    public void setAction(Actions action) {
        this.action = action;
    }

    public boolean getSystemState() {
        return systemState;
    }

    public void setSystemState(boolean systemState) {
        this.systemState = systemState;
    }
}
