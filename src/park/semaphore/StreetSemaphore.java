package park.semaphore;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Semaphore;

/**
 * This class is responsible for the semaphore
 */
public class StreetSemaphore implements Runnable {
    private Semaphore sem;
    private SharedSemaphore shared;

    /**
     * Default constructor
     *
     * @param shared the shared obj between the main and semaphore class
     * @param sem    the semaphore of this obj
     */
    public StreetSemaphore(SharedSemaphore shared, Semaphore sem) {
        this.shared = shared;
        this.sem = sem;
    }

    @Override
    public void run() {
        JFrame frame = new JFrame("Semaphore");
        Canvas c = new Canvas();
        frame.add(c);
        frame.setSize(200, 300);
        frame.setVisible(true);
        frame.setLocation(1030,  400);

        try {
            while (sem.availablePermits() >= 0) {
                sem.acquire();
                Thread.sleep(1000);
                c.setBackground(shared.getColor());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
