package park.semaphore;

import java.awt.*;

/**
 * Shared obj between Main and StreetSemaphore
 */
public class SharedSemaphore {
    private Color color;

    /**
     * The default constructor
     *
     * @param color the color of the canvas of the semaphore frame
     */
    public SharedSemaphore(Color color) {
        this.color = color;
    }

    /**
     * Returns the current color
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Setts the color
     *
     * @param color the color
     */
    public void setColor(Color color) {
        this.color = color;
    }
}
