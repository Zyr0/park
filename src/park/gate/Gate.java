package park.gate;

import javax.swing.*;
import java.util.concurrent.Semaphore;

/**
 * The class responsible for the gate
 */
public class Gate implements Runnable {
    private Semaphore sem, semMain;
    private SharedGate shared;

    /**
     * Default constructor
     *
     * @param shared the shared obj between the main and gate class
     * @param sem    the semaphore of this obj
     */
    public Gate(SharedGate shared, Semaphore sem, Semaphore semMain) {
        this.shared = shared;
        this.sem = sem;
        this.semMain = semMain;
    }

    @Override
    public void run() {
        JFrame frame = new JFrame("Gate");
        frame.setSize(200, 300);
        frame.setVisible(true);
        frame.setLocation(820, 400);

        JLabel label = new JLabel();
        frame.add(label);
        label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        try {
            while (sem.availablePermits() >= 0) {
                sem.acquire();
                Thread.sleep(1000);
                label.setText(shared.getState() ? "Gate Opened" : "Gate Closed");
                semMain.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

