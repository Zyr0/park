package park.gate;

/**
 * Shared obj between the Main and the Gate
 */
public class SharedGate {
    private boolean state;

    /**
     * Default constructor for the shared gate
     *
     * @param state the state of the gate open, close (true , false)
     */
    public SharedGate(boolean state) {
        this.state = state;
    }

    /**
     * Returns the current state of the gate open, close (true, false)
     *
     * @return the state
     */
    public boolean getState() {
        return state;
    }

    /**
     * Setts the current state of the gate open, close (true, false)
     *
     * @param state the state true, false (open, close)
     */
    public void setState(boolean state) {
        this.state = state;
    }
}
