package park.enums;

public enum Actions {
    CODE, CAR_ON, CAR_OFF, KEY, RESET, STOP
}
